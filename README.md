# The NetLogo hashing extension

With this NetLogo extension, which provides a wrapper around Scala's MurmurHash3
function
([scala.util.hashing.MurmurHash3](https://en.wikipedia.org/wiki/Hash_function),
 https://sites.google.com/site/murmurhash/,
https://github.com/aappleby/smhasher), you get access to a fast and powerful
[hash function](https://en.wikipedia.org/wiki/Hash_function) inside NetLogo.

A hash function maps data of arbitrary size to a value of fixed size. The
MurmurHash3 hash function returns random values that are uniformly distributed
and deterministic (given the same input data, always the same random value is
returned). MurmurHash3 is also computationally very efficient (i.e. fast).
There are some interesting applications of such a hash function for modeling
in NetLogo.

Please note that MurmurHash3 is not a [cryptographic hash
function](https://en.wikipedia.org/wiki/Cryptographic_hash_function), i.e. not
suitable for use in cryptography.

## How to install

Because of changes in the NetLogo API between version 6.0 and 6.1, there are
separate versions in separate branches called `api-v6.0` and `api-v6.1`. Please
select the branch according to your NetLogo version to acquire the hashing
extension (with the button inscribed `master`). Follow the instructions there
to download and install the correct zip file.
